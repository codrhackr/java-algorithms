package com.algos.string;

import java.util.Scanner;

public class StringReverse {

	public static void main (String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("enter the string to reverse: ");
		
		String input = scanner.nextLine();
		String output = reverseArray(input);
		
		System.out.println("Entered string -"+input+", reversed -"+output);
	}

	private static String reverseArray(String input) {

		int len = input.length();
		char []arr = input.toCharArray();
		for (int i =0; i < len/2; i++) {
			char temp = arr[i];
			arr[i] = arr[len-1-i];
			arr[len-1-i] = temp;
		}
		
		return new String(arr);
	}

}
