package com.algos.string;

import java.util.Scanner;
import java.util.Stack;

public class InfixToPrefix {

	private Stack<Character> mStack = new Stack<>();
	
	public String reverseString (String str) {
		int len = str.length();
		String rev = null;
		char []arr = str.toCharArray();
		char temp = ' ';
		for (int i = 0; i < len/2; i++) {
			if(arr[i] == '(')
				temp = ')';
			else if(arr[i] == ')')
				temp = '(';
			else
				temp = arr[i];
			
			if(arr[len-1-i] == '(')
				arr[i] = ')';
			else if(arr[len-1-i] == ')')
				arr[i] = '(';
			else
				arr[i] = arr[len-1-i];
			
			arr[len-1-i] = temp;
		}
		
		return new String (arr);
	}
	
	private boolean isValidDigit(char c) {
		if ((c >= '0' & c <='9') || (c >='a' && c <='f') 
				|| (c >= 'A' && c <='F'))
				return true;
		else
			return false;
	}
	
	private boolean isOperator(char c) {
		boolean ret = false;
		switch(c) {
		case '+':
		case '-':
		case '*':
		case '/':
		case '^':
			ret = true;
			break;
		}
		return ret;
	}
	
	private boolean isBraces (char c) {
		boolean ret = false;
		switch(c) {
		case '(':
		case '[':
		case '{':
			ret = true;
			break;
		}
		
		return ret;
	}
	
	private boolean isClosingBraces (char c) {
		boolean ret = false;
		switch(c) {
		case ')':
		case ']':
		case '}':
			ret = true; break;
		}
		return ret;
	}
	
	private char getOpeningBrace (char c) {
		char ret =' ';
		switch(c) {
		case ')':
			ret = '(';
			break;
		case ']':
			ret = '[';
			break;
		case '}':
			ret = '{';
			break;
		}
		
		return ret;
	}
	
	private int getOperatorPrecedence (char c) {
		int ret = 0;
		switch(c) {
		case '+':
		case '-':
			ret = 1;
			break;
		case '*':
		case '/':
			ret = 2; break;
		case '^':
			ret = 3; break;
		}
		return ret;
	}
	
	public String infixToPostfix(String exp) {
		
		int len = exp.length();
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < len; i++ ) {
			char c = exp.charAt(i);
			if(isValidDigit(c)) {
				System.out.println("Appending valid digit "+c);
				builder.append(c);
			} else if (isClosingBraces(c)) {
				while(!mStack.empty()) {
					if(mStack.peek() != getOpeningBrace(c)) {
						System.out.println("Appending ops from stack "+mStack.peek());
						builder.append(mStack.pop());
					} else 
						break;
				}
				if(!mStack.empty())
					mStack.pop();
			} else if (isOperator(c)) {
				System.out.println("Operator character - "+c);
				while(!mStack.empty()) {
					if (isBraces(mStack.peek()) || getOperatorPrecedence(c) > 
								getOperatorPrecedence(mStack.peek())) {
						System.out.println("exiting the loop - "+mStack.peek());
						break;
					}
					System.out.println("appending the content from stack - "+mStack.peek());
					builder.append(mStack.pop());
				}
				mStack.push(c);
			} else if (isBraces(c))
				mStack.push(c);
				
		}
		
		while(!mStack.empty())
			builder.append(mStack.pop());
		
		return builder.toString();
	}
	
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the infix expression: ");
		String infix = scanner.nextLine();
		InfixToPrefix itp = new InfixToPrefix();
		
		String reverse = itp.reverseString(infix);
		System.out.println("reversed exp - "+reverse);
		String postfix = itp.infixToPostfix(reverse);
		System.out.println("postfix exp - "+postfix);
		String prefix = itp.reverseString(postfix);
		
		System.out.println("Entered infix exp - "+infix);
		System.out.println("Converted Prefix exp - "+prefix);
	}

}
