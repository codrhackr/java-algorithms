package com.algos.string;

import java.util.Scanner;
import java.util.Stack;

public class ReverseWords {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("enter the line to reverse the words:");
		String line = scanner.nextLine();
		
		System.out.println("Entered line is -"+line);
		
		String revLine = reverseWords(line);
		System.out.println("Reveresed Line - "+revLine);

	}

	private static String reverseWords(String line) {
		
		Stack<Character> charStack = new Stack<>();
		StringBuilder builder = new StringBuilder();
		
		int len = line.length();
		for(int i =0; i<len; i++) {
			
			charStack.push(line.charAt(i));
			if(line.charAt(i) ==' ' || i == len-1) {
				while(!charStack.isEmpty()) {
					builder.append(charStack.pop());
				}
				builder.append(' ');
			}

			}
//		while(!charStack.isEmpty()) {
//			builder.append(charStack.pop());
//		}
		return builder.toString();
	}
	

}
