package com.algos.string;

import java.util.Scanner;
import java.util.Stack;

public class InfixToPostfix {

	private Stack <Character> mStack = new Stack<>();
	
	
	public boolean isValidDigit(char c) {
		boolean ret = false;
		switch(c) {
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'A':
		case 'a':
		case 'B':
		case 'b':
		case 'C':
		case 'c':
		case 'D':
		case 'd':
		case 'E':
		case 'e':
		case 'F':
		case 'f':
			ret = true;
			break;
		}
		return ret;
	}
	
	public boolean isOperator (char c) {
		boolean ret = false;
		switch(c) {
		case '+':
		case '-':
		case '*':
		case '/':
		case '^':
			ret = true;
			break;
		}
		return ret;
	}
	
	public boolean isClosingBrace(char c) {
		boolean ret = false;
		switch(c) {
		case ')':
		case ']':
		case '}':
			ret = true;
			break;
		}
		
		return ret;
	}
	
	public char getMatchingBrace(char c) {
		
		char ret = ' ';
		switch(c) {
		case ')':
			ret = '(';
			break;
		case ']':
			ret = '[';
			break;
		case '}':
			ret = '{';
			break;
		
		}
		return ret;
		
	}
	
	private boolean isOpeningBraces(char c) {
		boolean ret = false;
		switch(c) {
		case '(':
		case '[':
		case '{':
			ret = true; break;
		}
		return ret;
	}
	public int getOperatorPrecedence (char c) {
		int ret = 0;
		switch(c) {
		case '+':
		case '-':
			ret = 1;
			break;
		case '*':
		case '/':
			ret = 2;
			break;
		case '^':
			ret = 3;
			break;
		}
		
		return ret;
	}
	
	public String toPostOrder(String str) {
		int len = str.length();
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < len; i++) {
			char c = str.charAt(i);
			if(isValidDigit(c)) {
				builder.append(c);
			} else if (isClosingBrace(c)) {
				while(!mStack.empty() && mStack.peek() != getMatchingBrace(c)) {
					builder.append(mStack.pop());
				}
				if(!mStack.empty())
					mStack.pop();
			} else if (isOperator(c)) {
				while (!mStack.isEmpty()) {
					if (getOperatorPrecedence(c) > getOperatorPrecedence(mStack.peek())
							|| isOpeningBraces(mStack.peek())) {
						break;
					}
					builder.append(mStack.pop());
				}
				mStack.push(c);
			} else if(isOpeningBraces(c))
				mStack.push(c);
		}
		while (!mStack.empty())
			builder.append(mStack.pop());
		
		return builder.toString();
	}
	
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the infix expression: (-1 to exit)");
		while (true) {
			String infix = scanner.nextLine();
			if("-1".equals(infix)) {
				System.out.println("Exiting.. bye");
				break;
			}
			InfixToPostfix itp = new InfixToPostfix();
			String postfix = itp.toPostOrder(infix);
			System.out.println("Infix expression: "+infix);
			System.out.println("Postfix expression:"+postfix);
		}
		scanner.close();
	}

}
