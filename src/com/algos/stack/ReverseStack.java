package com.algos.stack;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.EmptyStackException;

public class ReverseStack <E>{

	private E [] mData;
	private int mTopOfStack;
	private int mStackSize;
	
	public ReverseStack(Class<E> clazz, int size){
	
		this.mStackSize = size;
		mData = (E[]) Array.newInstance(clazz, size);
		mTopOfStack = -1;
	}
	
	public void push (E data) {
		if (mTopOfStack < mStackSize) {
			mData[++mTopOfStack] = data;
		} else
			throw new StackOverflowError("Stack is full");
	}
	
	public E pop () {
		if(mTopOfStack >= 0)
			return mData[mTopOfStack--];
		else
			throw new EmptyStackException();
	}
	
	public boolean isEmpty() {

		return mTopOfStack < 0?true:false;
	}
	
	public void reverseStack() {
		
		if(isEmpty())
			return;
		
		E bottom = popBottom();
		reverseStack();
		push(bottom);
	}
	
	public E popBottom() {
		
		E top = pop();
		if(isEmpty())
			return top;
		else {
			
			E bottom = popBottom();
			push (top);
			return bottom;
		}
		
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(Arrays.toString(mData));
		
		return builder.toString();
	}
	

	public static void main(String []args) {
		ReverseStack <String> reverseStack = new ReverseStack<>(String.class, 10);
		
		reverseStack.push("Shibu");
		reverseStack.push("I");
		reverseStack.push("am");
		System.out.println(reverseStack);
		reverseStack.reverseStack();
		System.out.println(reverseStack);
		
		
	}
	
}
