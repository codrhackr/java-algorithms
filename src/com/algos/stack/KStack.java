package com.algos.stack;

import java.lang.reflect.Array;

public class KStack <E>{

	private int [] topOfStacks;
	private int [] nextIndex;
	private E [] mData;
	private int freeIndex;
	private int mStackCount;
	private int mStackSize;
	
	@SuppressWarnings("unchecked")
	public KStack (Class <E>clazz, int stackCount, int size) {
		this.mStackCount = stackCount;
		this.mStackSize = size * stackCount;
		mData = (E[])Array.newInstance(clazz, this.mStackSize);
		nextIndex = new int[mStackSize];
		topOfStacks = new int[mStackCount];
		for (int i = 0; i < mStackCount; i++)
			topOfStacks[i] = -1;
		for (int i = 0; i < (mStackSize -1); i++)
			nextIndex[i] = i+1;
		
		nextIndex[mStackSize-1] = -1;
		freeIndex = 0;
		
		
	}
	
	public void push(int stackNum, E data) {
		int index = freeIndex;
		freeIndex = nextIndex[index];
		nextIndex[index] = topOfStacks[stackNum];
		topOfStacks[stackNum] = index;
		mData[index] = data;
	}
	
	public E pop (int stackNum) {
		
		int index = topOfStacks[stackNum];
		topOfStacks[stackNum] = nextIndex[index];
		nextIndex[index] = freeIndex;
		freeIndex = index;
		
		return mData[index];
	}
	
	public static void main(String[] args) {

	}

}
