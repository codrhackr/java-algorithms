package com.algos.stack;

import java.util.Scanner;
import java.util.Stack;

public class CheckParenthesisBalanced {

	private Stack<Character> mStack = new Stack<>();
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the expression to evaluate ");
		String exp = scanner.nextLine();
		CheckParenthesisBalanced cpb = new CheckParenthesisBalanced();
		System.out.println("Expression is "+ (cpb.isExpressionBalanced(exp)==true?"balanced.":"unbalanced"));
		
	}

	private boolean isExpressionBalanced(String exp) {
		char[] arr = exp.toCharArray();
		boolean exitLoop = false;
		boolean balanced = true;
		for(int i=0; i <exp.length() && !exitLoop; i++) {
			switch(arr[i]) {
			case '(':
			case '{':
			case '[':
			case '<':
				mStack.push(arr[i]); break;
			case ')':
				if(mStack.empty()) {
					exitLoop = true;
					balanced = false;
				} else if(!mStack.empty() && mStack.pop() != '(') {
					exitLoop = true;
					balanced = false;
				}
				break;
			case '}':
				if(mStack.empty()) {
					exitLoop = true;
					balanced = false;
				} else if(!mStack.empty() && mStack.pop() != '{') {
					exitLoop = true;
					balanced = false;
				}
				break;
			case ']':
				
				if(mStack.empty()) {
					exitLoop = true;
					balanced = false;
				} else if(!mStack.empty() && mStack.pop() != '{') {
					exitLoop = true;
					balanced = false;
				}
				break;
			case '>':
				if(mStack.empty()) {
					exitLoop = true;
					balanced = false;
				} else if(!mStack.empty() && mStack.pop() != '<') {
					exitLoop = true;
					balanced = false;
				}
				break;
			
			}
		}
		if(mStack.isEmpty())
			return balanced;
		else 
			return false;
		
	}

	
}
