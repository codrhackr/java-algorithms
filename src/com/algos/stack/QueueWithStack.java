package com.algos.stack;

public class QueueWithStack {

	class Stack {
		private int mData[];
		private int mCapacity;
		private int mTopOfStack;
		
		Stack(int capacity) {
			this.mCapacity = capacity;
			this.mData = new int[this.mCapacity];
			this.mTopOfStack = -1;
		}
		
		void push (int data) {
			if (mCapacity > mTopOfStack) {
				mData[++mTopOfStack] = data;
			} else {
				throw new ArrayIndexOutOfBoundsException("Array is full!!");
			}
		}
		
		int pop () {
			if (mTopOfStack >= 0)
				return mData[mTopOfStack--];
			else
				throw new ArrayIndexOutOfBoundsException("Stack is empy!!!");
		}
		boolean empty () {
			return mTopOfStack >=0?false:true;
		}
		
	}
	private Stack dqStack;
	private Stack enqStack;
	private int mCapacity;
	
	public QueueWithStack (int capacity) {
		this.mCapacity = capacity;
		dqStack = new Stack(capacity);
		enqStack = new Stack(capacity);
	}
	
	public void enqueue (int val) {
		if (enqStack.empty()) {
			while(!dqStack.empty())
				enqStack.push(dqStack.pop());
		}
		enqStack.push(val);
	}
	
	public int dequeue () {
		if(dqStack.empty()) {
			while(!enqStack.empty())
				dqStack.push(enqStack.pop());
			
		}
		return dqStack.pop();
	}
	
	public static void main(String []args) {
		
		QueueWithStack qWithStack = new QueueWithStack(10);
		qWithStack.enqueue(10);
		qWithStack.enqueue(20);
		qWithStack.enqueue(30);
		System.out.println(qWithStack.dequeue());
		qWithStack.enqueue(100);
		qWithStack.enqueue(200);
		System.out.println(qWithStack.dequeue());
		System.out.println(qWithStack.dequeue());
		System.out.println(qWithStack.dequeue());
		System.out.println(qWithStack.dequeue());
		System.out.println(qWithStack.dequeue());
	}
}