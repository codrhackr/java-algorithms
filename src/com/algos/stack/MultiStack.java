package com.algos.stack;

import java.util.EmptyStackException;

public class MultiStack {
	
	private int mStack[];
	private int stackCapacity;
	private int topOfStack0;
	private int topOfStack1;
	
	public MultiStack(int capacity) {
		this.stackCapacity = capacity;
		this.topOfStack0 = -1;
		this.topOfStack1 = capacity;
		this.mStack = new int[capacity];
	}
	
	public void pushStack0 (int val) {
		
		if((topOfStack0 + 1) < topOfStack1)
			mStack[++topOfStack0] = val;
		else
			throw new ArrayIndexOutOfBoundsException("Stack is full...!!");
	}
	
	public void pushStack1 (int val) {
		if((topOfStack1 - 1)> topOfStack0)
			mStack[--topOfStack1] = val;
		else
			throw new ArrayIndexOutOfBoundsException("Stack is full...!!");
	}
	
	public int popStack0 () {
		if(this.topOfStack0 >= 0)
			return mStack[this.topOfStack0--];
		else
			throw new EmptyStackException();
	}
	
	public int popStack1 () {
		if(this.topOfStack1 <= stackCapacity) 
			return mStack[this.topOfStack1++];
		else
			throw new EmptyStackException();
	}
	
	public int peekStack0() {
		if(this.topOfStack0 >=0)
			return mStack[this.topOfStack0];
		else
			throw new EmptyStackException();
	}

	public int peekStack1() {
		if (this.topOfStack1 <= stackCapacity)
			return mStack[this.topOfStack1];
		else
			throw new EmptyStackException();
	}
	
	public static void main(String[] args) {

		MultiStack mstack = new MultiStack(5);
		mstack.pushStack0(10);
		mstack.pushStack0(20);
		mstack.pushStack0(30);
		mstack.pushStack1(99);
		mstack.pushStack1(88);
		mstack.pushStack1(77);
		System.out.println(mstack.peekStack0()+" - "+mstack.peekStack1());
		System.out.println(mstack.popStack0()+" - "+mstack.popStack1());
		System.out.println(mstack.peekStack0()+" - "+mstack.peekStack1());
	}

}
