package com.algos.stack;

import java.lang.reflect.Array;
import java.util.Arrays;

public class StackDeleteMiddle <E> {

	private E[] mData;
	private int mStackSize;
	private int mTopOfStack;
	
	public StackDeleteMiddle(Class<E> clazz, int size) {
		mStackSize = size;
		mData = (E[])Array.newInstance(clazz, size);
		mTopOfStack = -1;
	}
	
	public void push(E data) {
		if(mTopOfStack < mStackSize)
			mData[++mTopOfStack] = data;
		
	}
	
	public E pop () {
		return mData[mTopOfStack--];
	}
	
	public boolean isEmpty () {
		return mTopOfStack < 0? true:false;
	}
	
	public void deleteMid() {
		
		int middleIndex = mTopOfStack/2;
		deleteMid(middleIndex);
		
	}
	private void deleteMid(int middleIndex) {
		
		E val = pop();
		
		if(middleIndex > 0)
			deleteMid(middleIndex-1);
		
		if(middleIndex >0)
			push(val);
		trim();
	}

	private void trim() {
		mData[mTopOfStack+1] = null;
	}

	@Override
	public String toString() {
		int index = 0;
		StringBuilder builder = new StringBuilder();
		while (index <= mTopOfStack) {
			builder.append(mData[index++]+",  ");
		}
		return builder.toString();
	}
	public static void main(String[] args) {

		StackDeleteMiddle<Integer> stack = new StackDeleteMiddle<>(Integer.class, 10);
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		stack.push(5);
		stack.push(6);
		stack.push(7);
		System.out.println(stack);
		stack.deleteMid();
//		stack.pop();
//		stack.deleteMid();
		System.out.println(stack);
	}

}
