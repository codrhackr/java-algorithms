package com.algos.stack;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;
import java.util.Stack;

public class TowerOfHanoi <T> {

	Tower<T> createInstance(Class<T> clazz, String name) {
		Constructor ctor;
		Tower<T>instance = null;
		try {
			ctor = clazz.getConstructor(String.class);
			instance = (Tower<T>) ctor.newInstance(name);
			
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return instance;
	}
	class Tower <T> {
		
		private Stack <T> mStack = new Stack<>();
		private int mRingCount = 0;
		private String mTowerName;
		
		public Tower(String name) {
			mTowerName = name;
			mRingCount = 0;
		}
		
		public T getRing() {
			mRingCount--;
			return mStack.pop();
		}
		public int getCount() {
			return mRingCount;
		}
		
		public void addRing(T t) {
			mRingCount++;
			mStack.push(t);
		}

		public String getName() {
			return mTowerName;
		}
		@Override
		public String toString() {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(mTowerName+" -");
			int i = mRingCount - 1;
			while(i >= 0) {
				strBuilder.append(mStack.get(i));
				i--;
			}
			return strBuilder.toString();
		}

		public boolean isEmpty() {
			// TODO Auto-generated method stub
			return mStack.empty();
		}
	}

	public void solveTowerOfHanoi(Tower<T> fromTower, Tower<T> toTower) {
		Tower auxTower = new Tower("AuxTower");
		solveTowerOfHanoi(fromTower.getCount(), fromTower, toTower, auxTower);
	}
	
	private <T> void solveTowerOfHanoi(int ringCount, Tower<T> fromTower, 
			Tower <T> toTower, Tower<T> auxTower) {
		T t = null;
		if(ringCount == 1) {
			t = fromTower.getRing();
			System.out.println("Move "+t+" from "+fromTower.getName()+" to "+toTower.getName());
			toTower.addRing(t);
			return;
		}
		
		solveTowerOfHanoi(ringCount -1, fromTower, auxTower, toTower);
		t = fromTower.getRing();
		System.out.println("Move "+t+" from "+fromTower.getName()+" to "+toTower.getName());
		toTower.addRing(t);
		solveTowerOfHanoi(ringCount -1, auxTower, toTower, fromTower);
		
	}

	public static void main(String[] args) {

		TowerOfHanoi<String> toh = new TowerOfHanoi<>();
		
		TowerOfHanoi<String>.Tower<String> fromTower = toh.new Tower<>("fromTower");
		TowerOfHanoi<String>.Tower<String> toTower = toh.new Tower<>("toTower");

		fromTower.addRing("A");
		fromTower.addRing("B");
		fromTower.addRing("C");
		System.out.println("fromTower is "+fromTower.toString());
		toh.solveTowerOfHanoi(fromTower, toTower);
		System.out.println("toTower is "+toTower.toString());
		
	}

}
