package com.algos.stack;

import java.util.Scanner;

public class ArrayStack {
	
	private int[] mData;
	private int topOfStack;
	private int capacity;
	
	public ArrayStack(int capacity) {
		if (capacity >0) {
			mData = new int[capacity];
			this.capacity = capacity;
			topOfStack = -1;
		}
	}

	public void push (int data) {
		if(capacity > topOfStack) {
			mData[++topOfStack] = data;
		} else
			throw new ArrayIndexOutOfBoundsException("Stack is full!!!");
	}
	
	public int peek() {
		return mData[topOfStack];
	}
	
	public int pop () {
		if(topOfStack >=0)
			return mData[topOfStack--];
		else
			throw new ArrayIndexOutOfBoundsException("Stack is empty!!!");
	}
	
	public static void main(String[] args) {

		ArrayStack stack = new ArrayStack(10);
		
		stack.push(10);
		stack.push(20);
		stack.push(30);
		System.out.println(stack.pop());
		System.out.println("peek - "+stack.peek());
		
	}

}
