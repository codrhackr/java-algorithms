package com.algos.stack;

import java.lang.reflect.Array;
import java.util.EmptyStackException;

public class GenericStack <E> {
	
	private E mData[];
	private int mSize;
	private int mTopOfStack;
	public GenericStack(Class<E> clazz, int size) {
		mSize = size;
		mTopOfStack = -1;
		mData = (E[]) Array.newInstance(clazz, size);
	}
	
	public void push(E data) {
		if(mSize  > mTopOfStack)
			mData[++mTopOfStack] = data;
		else
			throw new StackOverflowError();
	}
	
	public E pop () {
		if(mTopOfStack >= 0)
			return mData[mTopOfStack--];
		else
			throw new EmptyStackException();
	}
	
	public static void main (String []args) {
		
		GenericStack<String> stringStack = new GenericStack<>(String.class, 10);
		GenericStack<Integer> intStack = new GenericStack<>(Integer.class, 10);
		stringStack.push("1");
		stringStack.push("2");
		stringStack.push("3");
		intStack.pop();
	}

}
