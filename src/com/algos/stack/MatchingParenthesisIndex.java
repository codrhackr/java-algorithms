package com.algos.stack;

import java.util.Scanner;
import java.util.Stack;

public class MatchingParenthesisIndex {
	
	Stack<Character> mStack = new Stack<>();
	
	private boolean isBracesMatching(char input, char closingBrace, char curr) {
		boolean match = false;
		if(mStack.empty()) {
			if(closingBrace == curr)
				return match = true;
			else {
				System.out.println("Unbalanced expression...");
				throw new IllegalArgumentException("Unbalanced expression...");
			}
		}
		
		if(mStack.pop() == input) {
			System.out.println("Opening brace popped from stack -"+closingBrace);
		} else {
			System.out.println("Unbalanced expression!!!");
			throw new IllegalArgumentException("Unbalanced expression...");
		}
		return match;
	}
	public int getMatchingBraceIndex(String exp, int braceIndex) {
		
		char brace = exp.charAt(braceIndex);
		switch(brace) {
		case '(':
		case '[':
		case '{':
			break;
		default:
			System.out.println("Invalid input index");
			throw new IllegalArgumentException("Input index invalid");
		}
		System.out.println("Character at input index - "+brace);
		int retIndex = -1;
		for (int i = braceIndex+1; i < exp.length() && retIndex<0; i++) {
			char temp = exp.charAt(i);
			System.out.println("Character at index["+i+"] is " +temp);
			switch(temp) {
			case '(':
			case '{':
			case '[':
				System.out.println("Opening brace pushed to stack - "+temp);
				mStack.push(temp);break;
			case ')':
			case '}':
			case ']':
				System.out.println("Closing brace checking for match - "+temp);
				switch(brace) {
				case '(':
					if(isBracesMatching(brace, ')', temp))
						retIndex = i;
					break;
				case '{':
					if(isBracesMatching(brace, '}', temp))
						retIndex = i;
					break;
				case '[':
					if(isBracesMatching(brace, ']', temp))
						retIndex = i;
					break;
				}
			}
		}
		if(retIndex < 0)
			throw new IllegalArgumentException("Cannot find matching brace");
		
		return retIndex;
		
	}

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("enter the expression");
		String exp = scanner.nextLine();
		System.out.println("Enter the index for the brace");
		int inputIndex = scanner.nextInt();
		MatchingParenthesisIndex mbi = new MatchingParenthesisIndex();
		int index = mbi.getMatchingBraceIndex(exp, inputIndex);
		System.out.println("Matching brace index - "+index);
		scanner.close();

	}

}
