package com.algos.stack;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

public class MaxParenthesisDepthStack {
	
	private Stack<Character> mStack = new Stack<>();

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the expression with paranthesis -");
		String exp = scanner.nextLine();
		MaxParenthesisDepthStack mpds = new MaxParenthesisDepthStack();
		
		Map<Character, Integer> result = mpds.getMaxDepth(exp);
		System.out.println(result.toString());
		
	}

	private Map<Character, Integer> getMaxDepth(String exp) {
		
		int len = exp.length();
		Map<Character, Integer> map = new HashMap<>();
		int maxCount = 0, currCount = 0;
		for(int index = 0; index < len; index++) {
			switch(exp.charAt(index)) {
			case '(':
				mStack.push(exp.charAt(index));
				currCount++;
				if(maxCount < currCount)
					maxCount = currCount;
				break;
			case ')':
				mStack.pop();
				currCount--;
				break;
			}
			
		}
		if(mStack.isEmpty())
			map.put('(', maxCount);
		else
			map.put('(', -1);
		return map;
	}

}
