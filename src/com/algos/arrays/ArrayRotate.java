package com.algos.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayRotate {

	public void rotateArray (int arr[], int size, int rotationOffset) {
		
		System.out.println(Arrays.toString(arr));
		reverseArray(arr, 0, rotationOffset-1);
		System.out.println(Arrays.toString(arr));
		reverseArray(arr, rotationOffset, size-1);
		System.out.println(Arrays.toString(arr));
		reverseArray(arr, 0, size-1);
		System.out.println(Arrays.toString(arr));
		
	}
	
	private void reverseArray(int [] arr, int startIndex, int endIndex) {
		
		int arrLen = endIndex - startIndex;
		for(int index = 0; index < arrLen/2; index++) {
			int temp = arr[index+startIndex];
			arr[index+startIndex] = arr[endIndex-index];
			arr[endIndex-index] = temp;
		}
		
	}
	
	public static void main(String []args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the array size - ");
		int size = scanner.nextInt();
		int arr[] = new int[size];
		
		int loop = 0;
		while (loop < size) {
			System.out.println("Enter the array values - ");
			arr[loop++] = scanner.nextInt();
			
		}
		System.out.println("Array value is "+Arrays.toString(arr));
		ArrayRotate arrayRotate = new ArrayRotate();
		System.out.println("Enter the offset for rotation:");
		int offset = scanner.nextInt();
		offset %=size;
		arrayRotate.rotateArray(arr, size, offset);
		System.out.println("Rotated array = "+Arrays.toString(arr));
	}
	
}
