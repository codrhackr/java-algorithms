package com.algos.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class MaxStock {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the size: ");
		int size = Integer.valueOf(scanner.nextLine());
		int stocks[] = new int[size];
		System.out.println("enter the stock prices: ");
		
		for(int i = 0; i < size; i++) {
			stocks[i] = scanner.nextInt();
		}
		System.out.println("entered array is "+Arrays.toString(stocks));
		
		MaxStock maxStock = new MaxStock();
		
		maxStock.maximize(stocks);

	}

	private void maximize(int[] stocks) {
		
		int buyingPrice = stocks[0];
		int sellingPrice = 0;
		int profit = -1;
		int buyingIndex = 0;
		int sellingIndex = -1;
		for (int i = 0; i < stocks.length-1; i++) {
			
			if(buyingPrice >stocks[i]) {
				buyingPrice = stocks[i];
				buyingIndex = i;
			}
			sellingPrice = stocks[i+1];
			
			int currProfit = sellingPrice - buyingPrice;
			if (profit < currProfit) {
				profit = currProfit;
				sellingIndex = i+1;
			}
		}
		
		System.out.println("Profit maximized - "+profit);
		if(profit > 0)
				System.out.println("Buy at ["+buyingIndex+"] sell at ["+sellingIndex+"]");
		
	}

}
