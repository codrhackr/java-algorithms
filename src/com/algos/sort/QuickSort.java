package com.algos.sort;

import java.util.Arrays;

public class QuickSort {

	private static final int ORDER_ASC = 1;
	private static final int ORDER_DSC = 2;
	private int order;

	public QuickSort(int order) {
		this.order = order;
	}

	public static void main(String[] args) {

		int arr[] = {10, 20, 50, 80, 90, 100, 70, 60, 40, 30};
		
		QuickSort qsort = new  QuickSort(QuickSort.ORDER_DSC);
		qsort.quickSort(arr);
		System.out.println("Sorted array - "+Arrays.toString(arr));
		
	}

	public void quickSort(int[] arr) {

		quickSort(arr, 0, arr.length-1);
		
	}

	private void quickSort(int[] arr, int low, int high) {

		if (low < high) {
			int pivot = 0;
			if(this.order == ORDER_ASC) {
				pivot = partitionAsc(arr, low, high);
			} else {
				pivot = partitionDsc(arr, low, high);
			}
			quickSort(arr, low, pivot-1);
			quickSort(arr, pivot+1, high);
		}
	}

	private int partitionDsc(int[] arr, int low, int high) {

		int pivot = arr[high];
		int temp = 0;
		int currIndex = low-1;
		for(int i = low; i <= high-1; i++) {
			if (pivot <= arr[i]) {
				currIndex++;
				temp = arr[currIndex];
				arr[currIndex] = arr[i];
				arr[i] = temp;
			}
		}
		temp = arr[currIndex+1];
		arr[currIndex+1] = pivot;
		arr[high] = temp;
		return currIndex+1;
	}

	private int partitionAsc(int[] arr, int low, int high) {

		int pivot = arr[high];
		int temp = 0;
		int currIndex = low-1;
		for (int i = low; i <= high-1; i++) {
			if (pivot >= arr[i]) {
				currIndex++;
				temp = arr[currIndex];
				arr[currIndex] = arr[i];
				arr[i] = temp;
			}
		}
		temp = arr[currIndex+1];
		arr[currIndex+1] = pivot;
		arr[high] = temp;
		return currIndex+1;
	}

}
