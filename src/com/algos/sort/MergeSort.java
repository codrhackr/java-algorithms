package com.algos.sort;

import java.util.Arrays;

public class MergeSort {
	
	public static final int SORT_ASC = 0;
	public static final int SORT_DSC = 1;
	
	private int mSortOrder;
	
	public MergeSort(int sortOrder) {
		this.mSortOrder = sortOrder;
	}
	
	public static void main (String [] args) {
		int arr[] = {300, 100, 200, 800, 1000, 900, 700, 500, 600, 400};
		
		MergeSort mergeSort = new MergeSort(SORT_DSC);
		mergeSort.mergeSort(arr);
		System.out.println("Sorted array - "+Arrays.toString(arr));
	}

	private void mergeSort(int[] arr) {

		mSort(arr, 0, arr.length-1);
	}

	private void mSort(int[] arr, int left, int right) {

		if (left < right) {
			int middle = (left+right)/2;
			mSort(arr, left, middle);
			mSort(arr, middle+1, right);
			
			merge(arr, left, middle, right);
		}
		
	}

	private void merge(int[] arr, int left, int middle, int right) {

		int lSize = middle-left+1;
		int rSize = right - middle;
		int []leftArr = new int[lSize];
		int []rightArr = new int[rSize];
		int i = 0, j = 0;
		while(i<lSize) {
			leftArr[i] = arr[left+i];
			i++;
		}
		
		while (j<rSize) {
			rightArr[j] = arr[middle+j+1];
			j++;
		}
		
		int k = left; i = 0; j = 0;
		if(SORT_ASC == this.mSortOrder) {
			while(i < lSize && j < rSize) {
				if(leftArr[i] >= rightArr[j]) {
					arr[k++] = rightArr[j++];
				} else {
					
					arr[k++] = leftArr[i++];
				}
			}
		} else {
			while (i < lSize && j < rSize) {
				if (leftArr[i] <= rightArr[j]) {
					arr[k++] = rightArr[j++];
				} else {
					arr[k++] = leftArr[i++];
				}
			}
		}
		
		while (i < lSize) {
			arr[k++] = leftArr[i++];
		}
		
		while (j < rSize) {
			arr[k++] = rightArr[j++];
		}
	}
	
}