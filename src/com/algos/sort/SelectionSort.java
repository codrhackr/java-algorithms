package com.algos.sort;

import java.util.Arrays;

public class SelectionSort {

	public static final int SORT_ASC = 0;
	public static final int SORT_DSC = 1;
	
	private int mOrder;
	
	public SelectionSort(int order) {
		this.mOrder = order;
	}
	
	public static void main (String []args) {
		int arr [] = {200, 400, 900, 1000, 100, 700, 800, 500, 600, 300};
		
		SelectionSort selectionSort = new SelectionSort(SORT_ASC);
		selectionSort.sort(arr);
		System.out.println("Sorted array is = "+Arrays.toString(arr));
	}

	private void sort(int[] arr) {

		for (int i =0; i < arr.length; i++) {
			int minIndx = i;
			for (int j = i+1; j < arr.length; j++) {
				if (arr[minIndx]> arr[j])
					minIndx = j;
			}
			int temp = arr[minIndx];
			arr[minIndx] = arr[i];
			arr[i] = temp;
		}
	}
	
}
