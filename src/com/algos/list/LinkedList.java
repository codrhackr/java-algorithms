package com.algos.list;

public class LinkedList <E> {

	class  Node<E> {
		private E mVal;
		private Node<E> next;
		private Node<E> prev;
		
		Node(E val) {
			mVal = val;
			prev = null;
			next = null;
		}
	}
	
	private Node<E> mHeadNode = null;
	private Node<E> mTailNode = null;
	private int mSize = 0;
	
	public void insert (E data) {
		Node<E> node = new Node<>(data);
		if(mHeadNode == null) {
			mHeadNode = node;
			mTailNode = node;
		}
		else {
			mTailNode.next = node;
			node.prev = mTailNode;
			mTailNode  = node;
		}
		mSize++;
			
	}
	
	public E remove () {
		return remove (0);
	}

	public E remove(int index) {
		if(index <0 || index > mSize)
			throw new IllegalArgumentException("Index is invalid "+index);
		Node<E> node = mHeadNode;
		while(index > 0) {
			node = node.next;
			index--;
		}
		if(node == mHeadNode) {
			if(null != node.next)
				mHeadNode = node.next;
			else
				mHeadNode = null;
		} else if(node.next != null) {
			node.prev.next = node.next;
			node.next.prev = node.prev;
		} else {
		
			mTailNode = node.prev;
			mTailNode.next = null;
		}
		node.next = null;
		node.prev = null;
		mSize--;
		return node.mVal;
	}
	
	@Override
	public String toString() {
		Node<E> node = mHeadNode;
		StringBuilder builder = new StringBuilder("[HEAD}->");
		while (null != node) {
			builder.append(node.mVal+"->");
			node = node.next;
			
		}
		builder.append("{null}");
		return builder.toString();
	}
	
	public static void main (String []args) {
		
		LinkedList<String> list = new LinkedList<>();
		list.insert("My");
		list.insert("name");
		list.insert("is");
		list.insert("Shibu");
		System.out.println(list.toString());
		list.remove(3);
		System.out.println(list.toString());
	}
}
